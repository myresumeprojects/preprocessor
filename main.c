#include <raylib.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

typedef struct type_word {
  char string[15];
  Vector2 position;
  Color color;
  int font_size;

} word;

int main(void) {
  InitWindow(1000, 600, "Secret Code");
  SetTargetFPS(30);

  srand(time(NULL));
  const int FONT_SIZE = 36;

  Font font = GetFontDefault();

  const char input_name[100] = "input.txt";
  const char output_name[100] = "output.txt";

  FILE *input;
  FILE *output;

  input = fopen(input_name, "r");
  output = fopen(output_name, "w");

  word text_message[100];
  for (int i = 0; i < 100; i++) {
    for (int j = 0; j < 15; j++) {
      text_message[i].string[j] = '\0';
    }
    text_message[i].position = (Vector2){0, 0};
    text_message[i].color = BLACK;
    text_message[i].font_size = FONT_SIZE;
  }

  int record = 1;
  int prev_record = 1;
  int is_secret = 0;
  int prev_secret = 0;
  int is_popup = 0;
  int prev_popup = 0;
  int is_sub = 0;
  int is_sup = 0;

  int x = 100;
  int y = 100;

  if (input != NULL) {
    int current_idx = 0;
    int temp_idx = 0;
    while (!feof(input)) {
      char c = getc(input);
      printf("%c", c);
      if (c == '\n') {
        text_message[current_idx].string[temp_idx] = '\n';
        Vector2 p = text_message[current_idx].position;
        text_message[current_idx].position = (Vector2){p.x + x, p.y + y};
        temp_idx = 0;
        current_idx++;
        x = 100;
        y += FONT_SIZE;
      } else if (c == ' ') {
        text_message[current_idx].string[temp_idx] = ' ';
        Vector2 p = text_message[current_idx].position;
        text_message[current_idx].position = (Vector2){p.x + x, p.y + y};
        x += MeasureText(text_message[current_idx].string, FONT_SIZE);
        temp_idx = 0;
        current_idx++;
      } else {
        if (c == '#') {
          record = !record;
          is_secret = !is_secret;
        }
        if (c == '*') {
          record = !record;
          is_popup = !is_popup;
        }
        if (c == '_') {
          record = !record;
          is_sub = !is_sub;
        }
        if (c == '^') {
          record = !record;
          is_sup = !is_sup;
        }
        if (c == '&') {
          record = !record;
          text_message[current_idx].color = GREEN;
        }
        if (is_sub && record == prev_record) {
          text_message[current_idx].font_size = FONT_SIZE / 2;
          text_message[current_idx].position.y = FONT_SIZE / 2.0f;
        }
        if (is_sup && record == prev_record) {
          text_message[current_idx].font_size = FONT_SIZE / 2;
        }
        if (is_secret && record == prev_record) {
          text_message[current_idx].color = RED;
        }
        if (is_popup && record == prev_record) {
          text_message[current_idx].color = BLUE;
        }
        if (record == prev_record) {
          text_message[current_idx].string[temp_idx] = c;
          temp_idx++;
        }
        prev_record = record;
        prev_secret = is_secret;
        prev_popup = is_popup;
      }
    }
  }

  for (int i = 0; i < 100; i++) {
    printf("%s", text_message[i].string);
  }

  SetTextLineSpacing(FONT_SIZE);

  while (!WindowShouldClose()) {

    BeginDrawing();

    ClearBackground(RAYWHITE);

    for (int i = 0; i < 100; i++) {
      DrawText((const char *)text_message[i].string, text_message[i].position.x,
               text_message[i].position.y, text_message[i].font_size,
               text_message[i].color);
    }

    EndDrawing();
  }

  fclose(input);
  fclose(output);
  CloseWindow();

  return 0;
}
